FROM node:16.20.2-alpine3.18 as build
COPY . ./app
WORKDIR /app
RUN npm install --include=dev
RUN npm run build

FROM node:16.20.2-alpine3.18
COPY --from=build ./app .
EXPOSE 8080
CMD npm start
