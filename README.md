# Overview
This project runs ansible-playbook which will install Docker and run 2048-game

## Requirements  
ansible-role-docker\
change the server address in /hosts/hosts

## 2048 Game Application

Open a web browser and point to [server_address:8080](http://server_address:8080/). You should see the `game-2048` welcome page:

![2048 Game Welcome Page](assets/images/game-2048-welcome-page.png)
